import glob
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import librosa
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from matplotlib.pyplot import specgram

#train_list = ['ai_data/bottle_2.mp3', 'ai_data/bottle_3.mp3', 'ai_data/bottle_4.mp3', 'ai_data/bottle_5.mp3','ai_data/bottle_6.mp3',
#              'ai_data/bottle_7.mp3', 'ai_data/bottle_8.mp3', 'ai_data/bottle_1.wav', 'ai_data/bottle_2.wav', 'ai_data/bottle_3.wav', 
#              'ai_data/bottle_4.wav', 'ai_data/bottle_5.wav', 'ai_data/bottle_6.wav', 'ai_data/bottle_7.wav', 'ai_data/bottle_8.wav', 
#              'ai_data/bottle_9.wav', 'ai_data/bottle_10.wav', 'ai_data/bottle_11.wav', 'ai_data/bottle_12.wav', 'ai_data/bottle_13.wav', 
#              'ai_data/bottle_14.wav', 'ai_data/bottle_15.wav', 'ai_data/bottle_16.wav', 'ai_data/bottle_17.wav', 'ai_data/bottle_18.wav', 
#              'ai_data/bottle_19.wav', 'ai_data/bottle_20.wav', 'ai_data/bottle_21.wav', 'ai_data/bottle_22.wav', 'ai_data/bottle_23.wav', 
#              'ai_data/bottle_24.wav', 'ai_data/bottle_25.wav',
              # end of bottle

#              'ai_data/can_2.mp3', 'ai_data/can_3.mp3', 'ai_data/can_4.mp3', 'ai_data/can_5.mp3', 'ai_data/can_6.mp3',
#              'ai_data/can_8.mp3', 'ai_data/can_9.mp3', 'ai_data/can_10.mp3', 'ai_data/can_11.mp3', 'ai_data/can_12.mp3',
#              'ai_data/can_14.mp3', 'ai_data/can_15.mp3', 'ai_data/can_1.wav', 'ai_data/can_2.wav', 'ai_data/can_3.wav', 
#              'ai_data/can_4.wav', 'ai_data/can_5.wav', 'ai_data/can_6.wav', 'ai_data/can_7.wav', 'ai_data/can_8.wav',
#              'ai_data/can_9.wav', 'ai_data/can_10.wav', 'ai_data/can_11.wav', 'ai_data/can_12.wav', 'ai_data/can_13.wav', 
#              'ai_data/can_14.wav', 'ai_data/can_15.wav', 'ai_data/can_16.wav', 'ai_data/can_17.wav', 'ai_data/can_18.wav', 
#              'ai_data/can_19.wav', 'ai_data/can_20.wav', 'ai_data/can_21.wav', 'ai_data/can_22.wav', 'ai_data/can_23.wav',
#              'ai_data/can_24.wav',
              # end of can

#              'ai_data/pet_2.mp3', 'ai_data/pet_3.mp3', 'ai_data/pet_4.mp3', 'ai_data/pet_6.mp3', 'ai_data/pet_7.mp3',
#              'ai_data/pet_8.mp3', 'ai_data/pet_9.mp3', 'ai_data/pet_10.mp3', 'ai_data/pet_12.mp3', 'ai_data/pet_13.mp3',
#              'ai_data/pet_14.mp3', 'ai_data/pet_1.wav', 'ai_data/pet_2.wav', 'ai_data/pet_3.wav', 'ai_data/pet_4.wav', 
#              'ai_data/pet_5.wav', 'ai_data/pet_6.wav', 'ai_data/pet_7.wav', 'ai_data/pet_8.wav', 'ai_data/pet_9.wav', 
#              'ai_data/pet_10.wav', 'ai_data/pet_11.wav', 'ai_data/pet_12.wav', 'ai_data/pet_13.wav', 'ai_data/pet_14.wav'
              # end of pet
#              ]

train_list = ['music/s1.mp3', 'music/s2.mp3', 'music/s3.mp3', 'music/s4.mp3', 'music/s5.mp3', 'music/s6.mp3',  
               # sad music
              'music/f1.flac', 'music/f2.flac', 'music/f3.flac', 'music/f4.flac', 'music/f5.flac'
               # happy music
             ]

num_of_train = len(train_list)
num_of_features = 193

x_train = [[0 for col in range(num_of_features)] for row in range(num_of_train)]


test_list = ['ai_data/bottle_1.mp3', 'ai_data/bottle_9.mp3',
             # bottle
             'ai_data/can_1.mp3', 'ai_data/can_7.mp3', 'ai_data/can_13.mp3',
             # can
             'ai_data/pet_1.mp3', 'ai_data/pet_5.mp3', 'ai_data/pet_11.mp3'
             # pet
             ]

num_of_test = len(test_list)
x_test = [[0 for col in range(num_of_features)] for row in range(num_of_test)]


#mfcc_train = []
#mfcc_test = []


#y_train = [[0], [0], [0], [0], [0], [0], [0], [0], [0], [0],
#           [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], 
#           [0], [0], [0], [0], [0], [0], [0], [0], [0], [0], 
#           [0], [0],                                         # num of bottle = 32
#           [1], [1], [1], [1], [1], [1], [1], [1], [1], [1],
#           [1], [1], [1], [1], [1], [1], [1], [1], [1], [1],
#           [1], [1], [1], [1], [1], [1], [1], [1], [1], [1],
#           [1], [1], [1], [1], [1], [1],                     # num of can = 36
#           [2], [2], [2], [2], [2], [2], [2], [2], [2], [2],
#           [2], [2], [2], [2], [2], [2], [2], [2], [2], [2],
#           [2], [2], [2], [2], [2]                           # num of bottle = 25 (train data : 30 -> 93)
#           ]
#num_of_class = 3

y_train = [[0], [0], [0], [0], [0], [0], 
           [1], [1], [1], [1], [1]]
num_of_class = 2

# y_test=[0,0,1,1,1,2,2,2]

# stft mean getting Short-time fourier transform.
def extract_feature(file_name):
    X, sample_rate = librosa.load(file_name)
    stft = np.abs(librosa.stft(X))  # abs function make return mean magnitude.
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
    mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
    return mfccs, chroma, mel, contrast, tonnetz


for i in train_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    for j in range(num_of_train):
        for k in range(num_of_features):
            x_train[j][k] = ext_features[k]
#    x_train.append(ext_features)
print("------------")
print(x_train[0])
print("------------")
print(x_train[1])
print("------------")
print(x_train[7])
print("------------")
print(x_train[8])

# normalize elemets in x_train
for j in range(num_of_train):
        for k in range(num_of_features):
            x_train[j][k] = (x_train[j][k] - np.mean(x_train[j])) / np.std(x_train[j])
#print("------------")
#print(x_train[0])
#print("------------")
#print(x_train[40])
#print("------------")
#print(x_train[90])
print("------------")
print(x_train[0])
print("------------")
print(x_train[1])
print("------------")
print(x_train[7])
print("------------")
print(x_train[8])

for i in test_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    for j in range(num_of_test):
        for k in range(num_of_features):
            x_test[j][k] = ext_features[k]

# normalize elemets in x_test
for j in range(num_of_test):
        for k in range(num_of_features):
            x_test[j][k] = ( x_test[j][k] - np.mean(x_test[j]) ) / np.std(x_test[j])
#    x_test.append(ext_features)


# placeholders for a tensor that will be always fed.
#X = tf.placeholder(tf.float32, shape=[None, num_of_features])
#Y = tf.placeholder(tf.float32, shape=[None, 1])
#W = tf.Variable(tf.random_normal([num_of_features, 1]), name='weight')
#b = tf.Variable(tf.random_normal([1]), name='bias')

X = tf.placeholder(tf.float32, shape=[None, num_of_features]) # none = num of rows in input array
Y = tf.placeholder(tf.int32, shape=[None,1]) # Y=0~2, shape=(none,1)
Y_one_hot = tf.one_hot(Y, num_of_class) # one hot, shape=(none,1,num_of_class)
# if original Y value was [2], then Y_one_hot will be [0,0,1]
# one hot function increases rank(dimension)
# in other word, if input indices are rank N, output will have rank N+1
# so you need to put it back by using reshape function
Y_one_hot = tf.reshape(Y_one_hot, [-1, num_of_class]) # shape=(none,num_of_class)

W = tf.Variable(tf.random_normal([num_of_features, num_of_class]), name='weight')
b = tf.Variable(tf.random_normal([num_of_class]), name='bias')

# tf.nn.softmax computes softmax activations
# softmax = exp(logits) / reduce_sum(exp(logits), dim)
# each index in return array of softmax function represents probability (sum = 1)
# if (0.7, 0.2, 0.1), then predicted value equals 0 with 70% probability 
logits = tf.matmul(X, W) + b
hypothesis = tf.nn.softmax(logits)

# Cross entropy cost/loss
cost_i = tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=Y_one_hot)
cost = tf.reduce_mean(cost_i)

# Minimize
optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.01)
train = optimizer.minimize(cost)

prediction = tf.argmax(hypothesis, 1)
# argmax(array,num) returns index number of element(s) with maximum value(s)
# if (0.7, 0.2, 0.1), then argmax returns 0 
correct_prediction = tf.equal(prediction, tf.argmax(Y_one_hot, 1))
# tf.argmax(Y_one_hot, 1) equals original Y before one hot processing
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# Launch the graph in a session.
sess = tf.Session()
# Initializes global variables in the graph.
sess.run(tf.global_variables_initializer())

# Launch graph
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for step in range(10000):
        sess.run(train, feed_dict={X: x_train, Y: y_train})
        if step % 1000 == 0:
            loss, acc = sess.run([cost, accuracy], feed_dict={X: x_train, Y: y_train})
            print("Step: {:5}\tLoss: {:.4f}\tAcc: {:.2%}".format(step, loss, acc))

    # Let's see if we can predict
    predicted_y = sess.run(prediction, feed_dict={X: x_train})
    # y_train: (N,1) = flatten => (N, ) matches predict.shape
    # [[1],[0]] -> [1,0]
    # myarray = np.asarray(mylist)
    # for p, y in zip(predicted_y, y_train.flatten()):
    for p, y in zip(predicted_y, np.asarray(y_train).flatten()):
        print("[{}] Prediction: {} True Y: {}".format(p == int(y), p, int(y)))


# for i in train_list:
#    y, sr = librosa.load(i)
#    y_slow = librosa.effects.time_stretch(y, 0.5)
#    y_pitch = librosa.effects.pitch_shift(y_slow, sr, n_steps=4)
#    mfcc_train.append(librosa.feature.mfcc(y=y_pitch, sr=sr))

# for i in test_list:
#    y, sr = librosa.load(i)
#    y_slow = librosa.effects.time_stretch(y, 0.5)
#    y_pitch = librosa.effects.pitch_shift(y_slow, sr, n_steps=4)
#    mfcc_test.append(librosa.feature.mfcc(y=y_pitch, sr=sr))

#for j in range(0, len(mfcc_train)):
#    temp = []
#    for i in range(0, len(mfcc_train[0])):
#        temp.append(np.mean(mfcc_train[j][i:i + 1, 0:len(mfcc_train[j][1])]))
#    x_train.append(temp)

#for j in range(0, len(mfcc_test)):
#    temp = []
#    for i in range(0, len(mfcc_test[0])):
#        temp.append(np.mean(mfcc_test[j][i:i + 1, 0:len(mfcc_test[j][1])]))
#    x_test.append(temp)


#print(len(x_train)) : 93
#print(len(x_train[0])) : 193

#from sklearn import linear_model
#logreg = linear_model.LogisticRegression()
#logreg.fit(x_train, y_train)
#y_test_estimated = logreg.predict(x_test)

#print(y_test_estimated)

#print(mfcc[:21,:1])
#print(np.mean(mfcc[:21,:1]))

import glob
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import librosa
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from matplotlib.pyplot import specgram

print("\n------------HY-auto_recylce powered by multi layer classification------------\n")

train_list = ['ai_data/bottle/bottle_5.mp3', 'ai_data/bottle/bottle_6.mp3', 'ai_data/bottle/bottle_7.mp3', 'ai_data/bottle/bottle_8.mp3',
              'ai_data/bottle/bottle_9.mp3', 'ai_data/bottle/bottle_10.mp3', 'ai_data/bottle/bottle_11.mp3', 'ai_data/bottle/bottle_12.mp3', 'ai_data/bottle/bottle_13.mp3',
              'ai_data/bottle/bottle_14.mp3', 'ai_data/bottle/bottle_15.mp3', 'ai_data/bottle/bottle_16.mp3', 'ai_data/bottle/bottle_17.mp3', 'ai_data/bottle/bottle_18.mp3',
              'ai_data/bottle/bottle_19.mp3', 'ai_data/bottle/bottle_20.mp3', 'ai_data/bottle/bottle_21.mp3', 'ai_data/bottle/bottle_22.mp3', 'ai_data/bottle/bottle_23.mp3',
              'ai_data/bottle/bottle_24.mp3', 'ai_data/bottle/bottle_25.mp3', 'ai_data/bottle/bottle_26.mp3', 'ai_data/bottle/bottle_27.mp3', 'ai_data/bottle/bottle_28.mp3',
              'ai_data/bottle/bottle_29.mp3', 'ai_data/bottle/bottle_30.mp3', 'ai_data/bottle/bottle_31.mp3', 'ai_data/bottle/bottle_32.mp3',
              # end of bottle

             'ai_data/can/can_5.mp3', 'ai_data/can/can_6.mp3', 'ai_data/can/can_7.mp3', 'ai_data/can/can_8.mp3',
             'ai_data/can/can_9.mp3', 'ai_data/can/can_10.mp3', 'ai_data/can/can_11.mp3', 'ai_data/can/can_12.mp3', 'ai_data/can/can_13.mp3',
             'ai_data/can/can_14.mp3', 'ai_data/can/can_15.mp3', 'ai_data/can/can_16.mp3', 'ai_data/can/can_17.mp3', 'ai_data/can/can_18.mp3',
             'ai_data/can/can_19.mp3', 'ai_data/can/can_20.mp3', 'ai_data/can/can_21.mp3', 'ai_data/can/can_22.mp3', 'ai_data/can/can_23.mp3',
             'ai_data/can/can_24.mp3', 'ai_data/can/can_25.mp3', 'ai_data/can/can_26.mp3', 'ai_data/can/can_27.mp3', 'ai_data/can/can_28.mp3',
             'ai_data/can/can_29.mp3', 'ai_data/can/can_30.mp3', 'ai_data/can/can_31.mp3', 'ai_data/can/can_32.mp3', 'ai_data/can/can_33.mp3',
             'ai_data/can/can_34.mp3', 'ai_data/can/can_35.mp3', 'ai_data/can/can_36.mp3', 'ai_data/can/can_37.mp3', 'ai_data/can/can_38.mp3',
             'ai_data/can/can_39.mp3', 'ai_data/can/can_40.mp3', 'ai_data/can/can_41.mp3', 'ai_data/can/can_42.mp3', 'ai_data/can/can_43.mp3',
             'ai_data/can/can_44.mp3', 'ai_data/can/can_45.mp3', 'ai_data/can/can_46.mp3', 'ai_data/can/can_47.mp3', 'ai_data/can/can_48.mp3',
             'ai_data/can/can_49.mp3', 'ai_data/can/can_50.mp3', 'ai_data/can/can_51.mp3', 'ai_data/can/can_52.mp3',
              # end of can

              'ai_data/pet/pet_5.mp3', 'ai_data/pet/pet_6.mp3', 'ai_data/pet/pet_7.mp3', 'ai_data/pet/pet_8.mp3',
              'ai_data/pet/pet_9.mp3', 'ai_data/pet/pet_10.mp3', 'ai_data/pet/pet_11.mp3', 'ai_data/pet/pet_12.mp3', 'ai_data/pet/pet_13.mp3',
              'ai_data/pet/pet_14.mp3', 'ai_data/pet/pet_15.mp3', 'ai_data/pet/pet_16.mp3', 'ai_data/pet/pet_17.mp3', 'ai_data/pet/pet_18.mp3',
              'ai_data/pet/pet_19.mp3', 'ai_data/pet/pet_20.mp3', 'ai_data/pet/pet_21.mp3', 'ai_data/pet/pet_22.mp3', 'ai_data/pet/pet_23.mp3',
              'ai_data/pet/pet_24.mp3', 'ai_data/pet/pet_25.mp3', 'ai_data/pet/pet_26.mp3', 'ai_data/pet/pet_27.mp3', 'ai_data/pet/pet_28.mp3',
              'ai_data/pet/pet_29.mp3', 'ai_data/pet/pet_30.mp3', 'ai_data/pet/pet_31.mp3', 'ai_data/pet/pet_32.mp3'
              # end of pet
              ]

test_list = ['ai_data/bottle/bottle_1.mp3','ai_data/bottle/bottle_2.mp3', 'ai_data/bottle/bottle_3.mp3', 'ai_data/bottle/bottle_4.mp3',
             'ai_data/bottle/bottle_33.mp3', 'ai_data/bottle/bottle_34.mp3', 'ai_data/bottle/bottle_35.mp3', 'ai_data/bottle/bottle_36.mp3',
             'ai_data/bottle/bottle_37.mp3',
             # bottle
             'ai_data/can/can_1.mp3', 'ai_data/can/can_2.mp3', 'ai_data/can/can_3.mp3', 'ai_data/can/can_4.mp3', 
             'ai_data/can/can_53.mp3','ai_data/can/can_54.mp3', 'ai_data/can/can_55.mp3', 'ai_data/can/can_56.mp3',
             'ai_data/can/can_57.mp3',
             # can
             'ai_data/pet/pet_1.mp3', 'ai_data/pet/pet_2.mp3', 'ai_data/pet/pet_3.mp3', 'ai_data/pet/pet_4.mp3', 
             'ai_data/pet/pet_33.mp3', 'ai_data/pet/pet_34.mp3', 'ai_data/pet/pet_35.mp3', 'ai_data/pet/pet_36.mp3', 'ai_data/pet/pet_37.mp3'
             # pet
             ]

num_of_train = len(train_list)
num_of_test = len(test_list)

#num_of_features_x = 193
#num_of_features_l1 = 130 # 193-63
#num_of_features_l2 = 67  # 193-63-63
#num_of_class = 3         # 193-63-63-64

num_of_features_x = 386
num_of_features_l1 = 258 # 193-63
num_of_features_l2 = 130 # 193-63-63
num_of_class = 3         # 193-63-63-64

x_train = []
x_test = []


#mfcc_train = []
#mfcc_test = []

y_train = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0,
                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                    1, 1, 1, 1, 1, 1, 1, 1,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2])

y_test = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0,
                   1, 1, 1, 1, 1, 1, 1, 1, 1,
                   2, 2, 2, 2, 2, 2, 2, 2, 2])

def leaky_relu(x):
    return tf.nn.relu(x) - 0.01 * tf.nn.relu(-x)

# stft mean getting Short-time fourier transform.
def extract_feature(file_name):
    X, sample_rate = librosa.load(file_name)
    X_stret = librosa.effects.time_stretch(X, 0.5)
    x_pitch = librosa.effects.pitch_shift(X_stret, sample_rate, n_steps=4)
    stft = np.abs(librosa.stft(X))  # abs function make return mean magnitude.
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
    mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
    std_mfccs = np.std(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
    std_chroma = np.std(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
    std_mel = np.std(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
    std_contrast = np.std(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
    std_tonnetz = np.std(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
    return mfccs, chroma, mel, contrast, tonnetz, std_mfccs, std_chroma, std_mel, std_contrast, std_tonnetz

print("\n-------Analyzing training data-------\n")

for i in train_list:
    mfccs, chroma, mel, contrast, tonnetz, std_mfccs, std_chroma, std_mel, std_contrast, std_tonnetz = extract_feature(i)
    ext_features_array = np.hstack([mfccs, chroma, mel, contrast, tonnetz, std_mfccs, std_chroma, std_mel, std_contrast, std_tonnetz])
    ext_features = ext_features_array.tolist()
    x_train.append(ext_features)

print("\n-------Training data has been analyzed!-------\n")

print("\n-------Analyzing test data-------\n")

for i in test_list:
    mfccs, chroma, mel, contrast, tonnetz, std_mfccs, std_chroma, std_mel, std_contrast, std_tonnetz = extract_feature(i)
    ext_features_array = np.hstack([mfccs, chroma, mel, contrast, tonnetz, std_mfccs, std_chroma, std_mel, std_contrast, std_tonnetz])
    ext_features = ext_features_array.tolist()
    x_test.append(ext_features)

print("\n-------Test data has been analyzed!-------\n")

# placeholders for a tensor that will be always fed.
X = tf.placeholder(tf.float32, shape=[None, num_of_features_x]) # None = num of rows in input array
Y = tf.placeholder(tf.float32)
Y_constant = tf.to_float(Y)

def multilayer_perceptron(x, weights, biases):
    # hidden layer
    layer_1 = tf.add(tf.matmul(x, weights['w1']), biases['b1'])
    layer_1 = leaky_relu(layer_1)
    # output layer
    out_layer = tf.matmul(layer_1, weights['w2']) + biases['b2']
    return out_layer

weights = {
    # weight is initialized by xavier initializer, which results better outcome comparing to just choosing random value
    # xavier initializer chooses random number between input value and output value and divides by root of input value
    'w1': tf.get_variable("w1", shape=[num_of_features_x, num_of_features_l1],initializer=tf.contrib.layers.xavier_initializer()),
    'w2': tf.get_variable("w2", shape=[num_of_features_l1, num_of_class],initializer=tf.contrib.layers.xavier_initializer())
}

biases = {
    'b1': tf.Variable(tf.zeros([num_of_features_l1])),
    'b2': tf.Variable(tf.zeros([num_of_class]))
}


# tf.nn.softmax computes softmax activations
# softmax = exp(logits) / reduce_sum(exp(logits), dim)
# each index in return array of softmax function represents probability (sum = 1)
# if (0.7, 0.2, 0.1), then predicted value equals 0 with 70% probability
logits = multilayer_perceptron(X, weights, biases)
pred_y = tf.nn.softmax(logits)
prediction = tf.argmax(pred_y, 1)
# argmax(array,num) returns index number of element(s) with maximum value(s)
# if (0.7, 0.2, 0.1), then argmax returns 0 

# Network         L1                             L2                             L3
# X -> (*) -> (+) -> (leaky_relu) -> (*) -> (+)  ->  (leaky_relu) -> (*) -> (+) -> (softmax) -> prediction
#       ^      ^                      ^      ^                        ^      ^ 
#       |      |                      |      |                        |      | 
#       w1     b1                     w2     b2                       w3     b3


# Cross entropy cost/loss
cost = tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels = Y)
cost = tf.reduce_mean(cost)

# Minimize cost
learning_rate = 0.01
optimizer = tf.train.AdamOptimizer(learning_rate)
train = optimizer.minimize(cost)

# Calculate accuracy
correct_prediction = tf.equal(prediction, tf.cast(Y_constant, tf.int64))
#correct_prediction = tf.equal(prediction, tf.argmax(Y_constant, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# Launch the graph in a session.
sess = tf.Session()

# Initializes global variables in the graph.
sess.run(tf.global_variables_initializer())

# Launch graph (start training)
print("\nTraining start!\n")
training_steps = 2000
printing_period = training_steps/10

sess = tf.Session()
sess.run(tf.global_variables_initializer())

for step in range(training_steps+1):
    sess.run(train, feed_dict={X: x_train, Y: y_train})
    if step % printing_period == 0:
        cos, acc = sess.run([cost, accuracy], feed_dict={X: x_train, Y: y_train})
        print("Training step: {:5}\tCost: {:.5f}\tAcc: {:.1%}".format(step, cos, acc))

print("\n-------Training complete!-------\n")

# Verify accuracy of constructed model
print("\n---------Testing start!---------\n")
predicted_y = sess.run(prediction, feed_dict={X: x_test})
num_of_success_in_test = 0
for p, y in zip(predicted_y, y_test):
    print("Test result: [{}] \tPrediction: {}\tTrue Y: {}".format(p == int(y), p, int(y)))
    if p == int(y):
        num_of_success_in_test = num_of_success_in_test+1
print("Test summary:\n\tnumber of test data = {}\n\tnumber of successfully predicted test data = {}\n\tpercentage of success = {:.2%}".format(num_of_success_in_test, len(y_test), num_of_success_in_test/len(y_test)))


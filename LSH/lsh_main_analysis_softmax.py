import glob
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
import librosa
import numpy as np
import tensorflow as tf
from auto_read_files import autoread_files

print("\n------------HY-auto_recylce powered by multi variable regression------------\n")

train_list=[]
y_train=[]
test_list=[]
y_test=[]
train_list,y_train = autoread_files('ai_data','bottle',train_list,y_train,0)
train_list,y_train = autoread_files('ai_data','pet',train_list,y_train,1)
train_list,y_train = autoread_files('ai_data','can',train_list,y_train,2)

test_list,y_test = autoread_files('ai_data'+os.path.sep+'test_set','bottle',test_list,y_test,0)
test_list,y_test = autoread_files('ai_data'+os.path.sep+'test_set','pet',test_list,y_test,1)
test_list,y_test = autoread_files('ai_data'+os.path.sep+'test_set','can',test_list,y_test,2)

x_train = []
x_test = []


num_of_train = len(train_list)
num_of_test = len(test_list)
num_of_features = 193
num_of_class = 3



def extract_feature(file_name):
    X, sample_rate = librosa.load(file_name)
    X_stret = librosa.effects.time_stretch(X, 0.5)
    x_pitch = librosa.effects.pitch_shift(X_stret, sample_rate, n_steps=4)
    stft = np.abs(librosa.stft(X))  # abs function make return mean magnitude.
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
    mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
    return mfccs, chroma, mel, contrast, tonnetz

print("\n-------Analyzing training data-------\n")

for i in train_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features_array = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    ext_features = ext_features_array.tolist()
    x_train.append(ext_features)


print("\n-------Training data has been analyzed!-------\n")

print("\n-------Analyzing test data-------\n")

for i in test_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features_array = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    ext_features = ext_features_array.tolist()
    x_test.append(ext_features)

print("\n-------Test data has been analyzed!-------\n")


X = tf.placeholder(tf.float32, shape=[None, num_of_features])
Y = tf.placeholder(tf.float32, shape=[None,num_of_class])


W = tf.Variable(tf.random_uniform([num_of_features, num_of_class]), name='weight')
b = tf.Variable(tf.zeros([num_of_class]), name='bias')


logits = tf.matmul(X, W) + b
pred_y = tf.nn.softmax(logits)


# Cross entropy cost/loss
cost_i = tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=Y)
cost = tf.reduce_mean(cost_i)


# Minimize cost
optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
train = optimizer.minimize(cost)

prediction = tf.argmax(pred_y, 1)
correct_prediction = tf.equal(prediction, tf.argmax(Y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# Launch the graph in a session.
sess = tf.Session()
# Initializes global variables in the graph.
sess.run(tf.global_variables_initializer())
print("\nTraining start!\n")
# Launch graph
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for step in range(100):
        sess.run(train, feed_dict={X: x_train, Y: y_train})
        if step % 100 == 0:
            loss, acc = sess.run([cost, accuracy], feed_dict={X: x_train, Y: y_train})
            print("Training step: {:5}\tLoss: {:.6f}\tAcc: {:.2%}".format(step, loss, acc))
    print("\n-------Training complete!-------\n---------Testing start!---------\n")
    predicted_y = sess.run(prediction, feed_dict={X: x_test})

    num_of_success_in_test = 0
    for p, y in zip(predicted_y, y_test):
        print("Test result: [{}] \tPrediction: {}\tTrue Y: {}".format(p == np.argmax(y, axis=-1), p, np.argmax(y, axis=-1)))
        if p == np.argmax(y, axis=-1):
            num_of_success_in_test = num_of_success_in_test+1
    print("Test summary:\n\tnumber of test data = {}\n\tnumber of successfully predicted test data = {}\n\tpercentage of success = {:.2%}".format(num_of_success_in_test, len(y_test), num_of_success_in_test/len(y_test)))

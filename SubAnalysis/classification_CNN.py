import glob
import os
import librosa
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
from auto_read_files import autoread_files_CNN

plt.style.use('ggplot')

frames = 60
bands = 41
feature_size = frames * bands  # 60x41

log_specgrams = []
log_specgrams2 = []


def windows(data, window_size):
    start = 0
    while start < len(data):
        yield start, start + window_size
        start += (window_size / 2)


def extract_features(file_name, labels, log_specgrams, bands=60, frames=41):
    window_size = 512 * (frames - 1)

    X, sample_rate = librosa.load(file_name)
    a = os.path.split(file_name)[1]
    if a.startswith('bottle'):
        class1 = [1, 0, 0]
    elif a.startswith('can'):
        class1 = [0, 1, 0]
    else:
        class1 = [0, 0, 1]
    for (start, end) in windows(X, window_size):
        if (len(X[int(start):int(end)]) == window_size):
            signal = X[int(start):int(end)]
            melspec = librosa.feature.melspectrogram(signal, n_mels=bands)
            logspec = librosa.logamplitude(melspec)
            logspec = logspec.T.flatten()[:, np.newaxis].T

            log_specgrams.append(logspec)
            labels.append(class1)


def one_hot_encode(labels):
    n_labels = len(labels)
    n_unique_labels = len(np.unique(labels))
    one_hot_encode = np.zeros((n_labels, n_unique_labels))
    one_hot_encode[np.arange(n_labels), labels] = 1
    return one_hot_encode

train_list=[]
train_label_list=[]
test_list=[]
test_label_list=[]
x_train=[]
x_test=[]
train_list,train_label_list = autoread_files_CNN('ai_data','bottle',train_list,train_label_list,0)
train_list,train_label_list = autoread_files_CNN('ai_data','can',train_list,train_label_list,1)
train_list,train_label_list = autoread_files_CNN('ai_data','pet',train_list,train_label_list,2)
test_list,test_label_list = autoread_files_CNN('ai_data\\test_set','bottle',test_list,test_label_list,0)
test_list,test_label_list = autoread_files_CNN('ai_data\\test_set','can',test_list,test_label_list,1)
test_list,test_label_list = autoread_files_CNN('ai_data\\test_set','pet',test_list,test_label_list,2)
train_label_list=[]
test_label_list=[]

for i in train_list:
    extract_features(i, train_label_list, log_specgrams, bands, frames)

for i in test_list:
    extract_features(i, test_label_list, log_specgrams2, bands, frames)

log_specgrams = np.asarray(log_specgrams).reshape(len(log_specgrams), bands, frames, 1)
features = np.concatenate((log_specgrams, np.zeros(np.shape(log_specgrams))), axis=3)
for i in range(len(features)):
    features[i, :, :, 1] = librosa.feature.delta(features[i, :, :, 0])

log_specgrams2 = np.asarray(log_specgrams2).reshape(len(log_specgrams2), bands, frames, 1)
features2 = np.concatenate((log_specgrams2, np.zeros(np.shape(log_specgrams2))), axis=3)

for i in range(len(features2)):
    features2[i, :, :, 1] = librosa.feature.delta(features2[i, :, :, 0])

x_train = np.array(features)
x_test = np.array(features2)

def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev = 0.1)
    return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(1.0, shape = shape)
    return tf.Variable(initial)

def conv2d(x, W):
    return tf.nn.conv2d(x,W,strides=[1,2,2,1], padding='SAME')

def apply_convolution(x,kernel_size,num_channels,depth):
    weights = weight_variable([kernel_size, kernel_size, num_channels, depth])
    biases = bias_variable([depth])
    return tf.nn.relu(tf.add(conv2d(x, weights),biases))

def apply_max_pool(x,kernel_size,stride_size):
    return tf.nn.max_pool(x, ksize=[1, kernel_size, kernel_size, 1],
                          strides=[1, stride_size, stride_size, 1], padding='SAME')

feature_size = frames*bands #60x41
num_labels = 3
num_channels = 2

batch_size = 50
kernel_size = 30
depth = 20
num_hidden = 100

learning_rate = 0.01
total_iterations = 200
X = tf.placeholder(tf.float32, shape=[None,bands,frames,num_channels])
Y = tf.placeholder(tf.float32, shape=[None,num_labels])

cov = apply_convolution(X,kernel_size,num_channels,depth)

shape = cov.get_shape().as_list()
cov_flat = tf.reshape(cov, [-1, shape[1] * shape[2] * shape[3]])

f_weights = weight_variable([shape[1] * shape[2] * depth, num_hidden])
f_biases = bias_variable([num_hidden])
f = tf.nn.relu(tf.add(tf.matmul(cov_flat, f_weights),f_biases))

keep_prob = tf.placeholder(tf.float32)
f_drop = tf.nn.dropout(f, keep_prob)

out_weights = weight_variable([num_hidden, num_labels])
out_biases = bias_variable([num_labels])
y_ = tf.matmul(f_drop, out_weights) + out_biases

#oss = -tf.reduce_sum(Y * tf.log(y_))
#optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)

loss = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits(logits=y_, labels=Y))
optimizer = tf.train.AdamOptimizer(0.01).minimize(loss)

correct_pred = tf.equal(tf.argmax(y_, 1), tf.argmax(Y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

cost_history = np.empty(shape=[1], dtype=float)
init = tf.global_variables_initializer()
with tf.Session() as session:
    session.run(init)

    for itr in range(total_iterations):
        offset = (itr * batch_size) % (np.array(train_label_list).shape[0] - batch_size)
        temp = offset + batch_size
        batch_x = x_train[offset:temp, :, :, :]
        batch_y = np.array(train_label_list)[offset:(offset + batch_size), :]

        _, c = session.run([optimizer, loss], feed_dict={X: batch_x, Y: batch_y, keep_prob: 0.5})
        cost_history = np.append(cost_history, c)

    test_acc = session.run(accuracy, feed_dict={
        X: batch_x, Y: batch_y, keep_prob: 0.5})
    print('Testing Accuracy: {}'.format(test_acc))
    y_pred = session.run(tf.argmax(y_, 1), feed_dict={X: x_test, keep_prob: 0.5})
    y_true = session.run(tf.argmax(test_label_list, 1))

    print(y_pred)
    print(y_true)

import matplotlib.pyplot as plt
fig = plt.figure(figsize=(10,8))
plt.plot(cost_history)
plt.axis([0,(total_iterations),0,max(cost_history)])
plt.show()
import os
import librosa
import numpy as np
from auto_read_files import autoread_files

def extract_feature(file_name):
    X, sample_rate = librosa.load(file_name)
    X_stret = librosa.effects.time_stretch(X, 0.5)
    X_pitch = librosa.effects.pitch_shift(X_stret, sample_rate, n_steps=4)
    # these function is used to improve feature_extracting but now, accuracy is 100% so i din't use yet.
    stft = np.abs(librosa.stft(X_pitch))
    mfccs = np.mean(librosa.feature.mfcc(y=X_pitch, sr=sample_rate, n_mfcc=40).T, axis=0)
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
    mel = np.mean(librosa.feature.melspectrogram(X_pitch, sr=sample_rate).T, axis=0)
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)

    return mfccs,chroma,mel,contrast,tonnetz
# stft mean getting Short-time fourier transform.

train_list=[]
y_train=[]
test_list=[]
y_test=[]
train_list,y_train = autoread_files('ai_data','bottle',train_list,y_train,0)
train_list,y_train = autoread_files('ai_data','pet',train_list,y_train,1)
train_list,y_train = autoread_files('ai_data','can',train_list,y_train,2)

test_list,y_test = autoread_files('ai_data'+os.path.sep+'test_set','bottle',test_list,y_test,0)
test_list,y_test = autoread_files('ai_data'+os.path.sep+'test_set','pet',test_list,y_test,1)
test_list,y_test = autoread_files('ai_data'+os.path.sep+'test_set','can',test_list,y_test,2)

x_train = []
x_test = []

for i in train_list:

    mfccs,chroma,mel,contrast,tonnetz = extract_feature(i)
    ext_features = np.hstack([mfccs,chroma,mel,contrast,tonnetz])
    x_train.append(ext_features)

for i in test_list:
    mfccs,chroma,mel,contrast,tonnetz = extract_feature(i)
    ext_features = np.hstack([mfccs,chroma,mel,contrast,tonnetz])
    x_test.append(ext_features)

from sklearn.svm import SVC

svm = SVC(kernel='rbf', C=1.0, random_state=0)
svm.fit(x_train, y_train)
# SVM의 y_test는 ANN과는 달리 ont hot vector 가 아닌 평범한 모양의 벡터임. e.g [0,0,0,1,1,1,2,2,2]
# 이 또한 자동화하기 위해 auto_read_file 함수와  auto_read_file_oen_hot 함수를 각각 선언 (in auto_read_files.py)

y_pred_svc = svm.predict(x_test)
count = (y_pred_svc == y_test)
# 평균 정확도는 76% 산출

print(y_test)
print(y_pred_svc)
print(y_test==y_pred_svc)
print("=========================")
print(((count.sum()-(count==False).sum())/count.sum())*100)
print("=========================")
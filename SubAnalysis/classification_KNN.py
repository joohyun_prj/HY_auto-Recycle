import os
import librosa
import numpy as np
from auto_read_files import autoread_files
from sklearn.neighbors import KNeighborsClassifier

def extract_feature(file_name):
    X, sample_rate = librosa.load(file_name)
    X_stret = librosa.effects.time_stretch(X, 0.5)
    X_pitch = librosa.effects.pitch_shift(X_stret, sample_rate, n_steps=4)
    #these function is used to improve feature_extracting but now, accuracy is 100% so i din't use yet.
    stft = np.abs(librosa.stft(X_pitch))
    mfccs = np.mean(librosa.feature.mfcc(y=X_pitch, sr=sample_rate, n_mfcc=40).T, axis=0)
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
    mel = np.mean(librosa.feature.melspectrogram(X_pitch, sr=sample_rate).T, axis=0)
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)

    return mfccs,chroma,mel,contrast,tonnetz
# stft mean getting Short-time fourier transform.

train_list=[]
y_train=[]
test_list=[]
y_test=[]
train_list,y_train = autoread_files('ai_data','bottle',train_list,y_train,0)
train_list,y_train = autoread_files('ai_data','pet',train_list,y_train,1)
train_list,y_train = autoread_files('ai_data','can',train_list,y_train,2)

test_list,y_test = autoread_files('ai_data'+os.path.sep+'test_set','bottle',test_list,y_test,0)
test_list,y_test = autoread_files('ai_data'+os.path.sep+'test_set','pet',test_list,y_test,1)
test_list,y_test = autoread_files('ai_data'+os.path.sep+'test_set','can',test_list,y_test,2)

x_train = []
x_test = []

for i in train_list:

    mfccs,chroma,mel,contrast,tonnetz = extract_feature(i)
    ext_features = np.hstack([mfccs,chroma,mel,contrast,tonnetz])
    x_train.append(ext_features)

for i in test_list:
    mfccs,chroma,mel,contrast,tonnetz = extract_feature(i)
    ext_features = np.hstack([mfccs,chroma,mel,contrast,tonnetz])
    x_test.append(ext_features)

neigh = KNeighborsClassifier(n_neighbors=3)
neigh.fit(x_test, y_test)
# K-nearest neighbors 도 SVM과 같이 input으로 one hot vector가 아닌 일반 vector를 받음
# SVM과 비교해서 특별히 추가적인 작업으 필요없음
# KNN은 지도학습으로 predict하고자 하는 데이터 지점에서 일정거리 안에 가장 많이
# 포함되어 있는 class에 속하게되는 알고리즘을 가지고 있음.

pred = neigh.predict(x_test)
# 평균 정확도는 86% 산출

count = (y_test==pred)
print("=========================")
print(((count.sum()-(count==False).sum())/count.sum())*100)
print("=========================")
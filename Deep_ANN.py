import glob
import os
import librosa
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from matplotlib.pyplot import specgram

train_list = ['ai_data/bottle_2.mp3', 'ai_data/bottle_3.mp3', 'ai_data/bottle_4.mp3', 'ai_data/bottle_5.mp3',
              'ai_data/bottle_6.mp3', 'ai_data/bottle_7.mp3', 'ai_data/bottle_8.mp3', 'ai_data/bottle_10.mp3',
              'ai_data/bottle_12.mp3', 'ai_data/bottle_13.mp3',
              # end of bottle

              'ai_data/pet_2.mp3', 'ai_data/pet_3.mp3', 'ai_data/pet_4.mp3', 'ai_data/pet_6.mp3', 'ai_data/pet_7.mp3',
              'ai_data/pet_8.mp3', 'ai_data/pet_9.mp3', 'ai_data/pet_10.mp3', 'ai_data/pet_12.mp3',
              'ai_data/pet_13.mp3', 'ai_data/pet_14.mp3', 'ai_data/pet_15.mp3', 'ai_data/pet_16.mp3',
              'ai_data/pet_18.mp3', 'ai_data/pet_19.mp3', 'ai_data/pet_22.mp3',
              'ai_data/pet_23.mp3', 'ai_data/pet_24.mp3','ai_data/pet_25.mp3',
              # end of pet

              'ai_data/can_2.mp3', 'ai_data/can_3.mp3', 'ai_data/can_4.mp3', 'ai_data/can_5.mp3', 'ai_data/can_6.mp3',
              'ai_data/can_8.mp3', 'ai_data/can_9.mp3', 'ai_data/can_10.mp3', 'ai_data/can_11.mp3',
              'ai_data/can_12.mp3', 'ai_data/can_14.mp3', 'ai_data/can_15.mp3', 'ai_data/can_17.mp3',
              'ai_data/can_18.mp3', 'ai_data/can_19.mp3', 'ai_data/can_20.mp3', 'ai_data/can_22.mp3',
              'ai_data/can_23.mp3', 'ai_data/can_24.mp3', 'ai_data/can_25.mp3', 'ai_data/can_26.mp3',
              # end of can

              ]

test_list = [
            'ai_data/bottle_1.mp3', 'ai_data/bottle_9.mp3', 'ai_data/bottle_11.mp3',
            # bottle

            'ai_data/pet_1.mp3', 'ai_data/pet_5.mp3', 'ai_data/pet_11.mp3', 'ai_data/pet_17.mp3', 'ai_data/pet_21.mp3',
            'ai_data/pet_20.mp3',
            # pet

            'ai_data/can_1.mp3', 'ai_data/can_7.mp3', 'ai_data/can_13.mp3', 'ai_data/can_16.mp3', 'ai_data/can_21.mp3',
            'ai_data/can_27.mp3'
            # can

             ]

mfcc_train = []
mfcc_test = []
x_train = []
y_train = [[1,0,0], [1,0,0], [1,0,0], [1,0,0], [1,0,0], [1,0,0], [1,0,0], [1,0,0], [1,0,0], [1,0,0],  # bottle

           [0,1,0], [0,1,0], [0,1,0], [0,1,0], [0,1,0], [0,1,0], [0,1,0], [0,1,0], [0,1,0], [0,1,0], # pet
           [0,1,0], [0,1,0], [0,1,0], [0,1,0], [0,1,0], [0,1,0], [0,1,0], [0,1,0], [0,1,0],  # pet

           [0,0,1], [0,0,1], [0,0,1], [0,0,1], [0,0,1], [0,0,1], [0,0,1], [0,0,1], [0,0,1], [0,0,1], # can
           [0,0,1], [0,0,1], [0,0,1], [0,0,1], [0,0,1], [0,0,1], [0,0,1], [0,0,1], [0,0,1], [0,0,1], # can
           [0,0,1] # can
           ]
x_test = []


y_test=[[0,0,0],[0,0,0],[0,0,0],
        [0,1,0],[0,1,0],[0,1,0],[0,1,0],[0,1,0],[0,1,0],
        [0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]
        ]

# stft mean getting Short-time fourier transform.
def extract_feature(file_name):
    X, sample_rate = librosa.load(file_name)
    #X_stret = librosa.effects.time_stretch(X, 0.5)
    #X_pitch = librosa.effects.pitch_shift(X_stret, sample_rate, n_steps=4)
    # these function is used to improve feature_extracting but now, accuracy is 100% so i din't use yet.
    stft = np.abs(librosa.stft(X))  # abs function make return mean magnitude.
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
    mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
    return mfccs, chroma, mel, contrast, tonnetz


for i in train_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    x_train.append(ext_features)

for i in test_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    x_test.append(ext_features)

# for i in train_list:
#    y, sr = librosa.load(i)
#    y_slow = librosa.effects.time_stretch(y, 0.5)
#    y_pitch = librosa.effects.pitch_shift(y_slow, sr, n_steps=4)
#    mfcc_train.append(librosa.feature.mfcc(y=y_pitch, sr=sr))

# for i in test_list:
#    y, sr = librosa.load(i)
#    y_slow = librosa.effects.time_stretch(y, 0.5)
#    y_pitch = librosa.effects.pitch_shift(y_slow, sr, n_steps=4)
#    mfcc_test.append(librosa.feature.mfcc(y=y_pitch, sr=sr))

# for j in range(0, len(mfcc_train)):
#     temp = []
#     for i in range(0, len(mfcc_train[0])):
#         temp.append(np.mean(mfcc_train[j][i:i + 1, 0:len(mfcc_train[j][1])]))
#     x_train.append(temp)
#
# for j in range(0, len(mfcc_test)):
#     temp = []
#     for i in range(0, len(mfcc_test[0])):
#         temp.append(np.mean(mfcc_test[j][i:i + 1, 0:len(mfcc_test[j][1])]))
#     x_test.append(temp)

tr_features, tr_labels = x_train, y_train
ts_features, ts_labels = x_test, y_test

training_epochs = 5000
n_dim = len(tr_features[0])
n_classes = 3
n_hidden_units_one = 280
n_hidden_units_two = 300
sd = 1 / np.sqrt(n_dim)
learning_rate = 0.01

X = tf.placeholder(tf.float32,[None,n_dim])
Y = tf.placeholder(tf.float32,[None,n_classes])

W_1 = tf.Variable(tf.random_normal([n_dim,n_hidden_units_one], mean = 0, stddev=sd))
b_1 = tf.Variable(tf.random_normal([n_hidden_units_one], mean = 0, stddev=sd))
h_1 = tf.nn.tanh(tf.matmul(X,W_1) + b_1)


W_2 = tf.Variable(tf.random_normal([n_hidden_units_one,n_hidden_units_two], mean = 0, stddev=sd))
b_2 = tf.Variable(tf.random_normal([n_hidden_units_two], mean = 0, stddev=sd))
h_2 = tf.nn.sigmoid(tf.matmul(h_1,W_2) + b_2)


W = tf.Variable(tf.random_normal([n_hidden_units_two,n_classes], mean = 0, stddev=sd))
b = tf.Variable(tf.random_normal([n_classes], mean = 0, stddev=sd))
y_ = tf.nn.softmax(tf.matmul(h_2,W) + b)

init = tf.global_variables_initializer()

cost_function = -tf.reduce_sum(Y * tf.log(y_))
optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost_function)

correct_prediction = tf.equal(tf.argmax(y_,1), tf.argmax(Y,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

y_true, y_pred = None, None
with tf.Session() as sess:
    sess.run(init)
    for epoch in range(training_epochs):
        _, cost = sess.run([optimizer, cost_function], feed_dict={X: np.array(tr_features), Y: np.array(tr_labels)})

    y_pred = sess.run(tf.argmax(y_, 1), feed_dict={X: ts_features})
    y_true = sess.run(tf.argmax(ts_labels, 1))
    #print('Test accuracy: ', round(sess.run(accuracy, feed_dict={X: ts_features, Y: ts_labels}), 2))

print(y_true)
print("+++++++++++++++++++++++++++++++++++++++++++++++++")
print(y_pred)
import glob
import os
import librosa
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from matplotlib.pyplot import specgram

train_list = ['ai_data/bottle_2.mp3', 'ai_data/bottle_3.mp3', 'ai_data/bottle_4.mp3', 'ai_data/bottle_5.mp3',
              'ai_data/bottle_6.mp3', 'ai_data/bottle_7.mp3', 'ai_data/bottle_8.mp3', 'ai_data/bottle_10.mp3',
              'ai_data/bottle_12.mp3', 'ai_data/bottle_13.mp3', 'ai_data/bottle_14.mp3', 'ai_data/bottle_15.mp3',
              'ai_data/bottle_16.mp3', 'ai_data/bottle_17.mp3', 'ai_data/bottle_18.mp3', 'ai_data/bottle_19.mp3',
              'ai_data/bottle_20.mp3', 'ai_data/bottle_21.mp3', 'ai_data/bottle_22.mp3', 'ai_data/bottle_23.mp3',
              'ai_data/bottle_24.mp3', 'ai_data/bottle_25.mp3', 'ai_data/bottle_26.mp3', 'ai_data/bottle_27.mp3',
              'ai_data/bottle_29.mp3', 'ai_data/bottle_30.mp3', 'ai_data/bottle_31.mp3', 'ai_data/bottle_32.mp3',
              'ai_data/bottle_34.mp3', 'ai_data/bottle_35.mp3', 'ai_data/bottle_36.mp3', 'ai_data/bottle_37.mp3',

              # end of bottle

              'ai_data/pet_2.mp3', 'ai_data/pet_3.mp3', 'ai_data/pet_4.mp3', 'ai_data/pet_6.mp3', 'ai_data/pet_7.mp3',
              'ai_data/pet_8.mp3', 'ai_data/pet_9.mp3', 'ai_data/pet_10.mp3', 'ai_data/pet_12.mp3',
              'ai_data/pet_13.mp3', 'ai_data/pet_14.mp3', 'ai_data/pet_15.mp3', 'ai_data/pet_16.mp3',
              'ai_data/pet_18.mp3', 'ai_data/pet_19.mp3', 'ai_data/pet_22.mp3', 'ai_data/pet_23.mp3',
              'ai_data/pet_24.mp3', 'ai_data/pet_25.mp3', 'ai_data/pet_26.mp3', 'ai_data/pet_27.mp3',
              'ai_data/pet_28.mp3', 'ai_data/pet_29.mp3', 'ai_data/pet_30.mp3', 'ai_data/pet_31.mp3',
              'ai_data/pet_32.mp3', 'ai_data/pet_33.mp3', 'ai_data/pet_34.mp3', 'ai_data/pet_35.mp3',
              'ai_data/pet_36.mp3', 'ai_data/pet_37.mp3',
              # end of pet

              'ai_data/can_2.mp3', 'ai_data/can_3.mp3', 'ai_data/can_4.mp3', 'ai_data/can_5.mp3', 'ai_data/can_6.mp3',
              'ai_data/can_8.mp3', 'ai_data/can_9.mp3', 'ai_data/can_10.mp3', 'ai_data/can_11.mp3',
              'ai_data/can_12.mp3', 'ai_data/can_14.mp3', 'ai_data/can_15.mp3', 'ai_data/can_17.mp3',
              'ai_data/can_18.mp3', 'ai_data/can_19.mp3', 'ai_data/can_20.mp3', 'ai_data/can_22.mp3',
              'ai_data/can_23.mp3', 'ai_data/can_24.mp3', 'ai_data/can_25.mp3', 'ai_data/can_26.mp3',
              'ai_data/can_27.mp3', 'ai_data/can_28.mp3', 'ai_data/can_29.mp3', 'ai_data/can_30.mp3',
              'ai_data/can_31.mp3', 'ai_data/can_32.mp3', 'ai_data/can_33.mp3', 'ai_data/can_34.mp3',
              'ai_data/can_35.mp3', 'ai_data/can_36.mp3', 'ai_data/can_37.mp3', 'ai_data/can_38.mp3',
              'ai_data/can_39.mp3', 'ai_data/can_40.mp3', 'ai_data/can_41.mp3', 'ai_data/can_42.mp3',
              'ai_data/can_43.mp3', 'ai_data/can_45.mp3',
              'ai_data/can_47.mp3', 'ai_data/can_48.mp3', 'ai_data/can_49.mp3', 'ai_data/can_50.mp3',
              'ai_data/can_51.mp3', 'ai_data/can_52.mp3', 'ai_data/can_53.mp3', 'ai_data/can_54.mp3',
              'ai_data/can_55.mp3', 'ai_data/can_56.mp3', 'ai_data/can_57.mp3',

              # end of can

              ]

test_list = [
    'ai_data/bottle_1.mp3', 'ai_data/bottle_9.mp3', 'ai_data/bottle_11.mp3', 'ai_data/bottle_24.mp3',
    'ai_data/bottle_25.mp3',
    'ai_data/bottle_29.mp3', 'ai_data/bottle_33.mp3',
    # bottle

    'ai_data/pet_1.mp3', 'ai_data/pet_5.mp3', 'ai_data/pet_11.mp3', 'ai_data/pet_17.mp3', 'ai_data/pet_21.mp3',
    'ai_data/pet_20.mp3', 'ai_data/pet_38.mp3', 'ai_data/pet_39.mp3',
    # pet

    'ai_data/can_1.mp3', 'ai_data/can_7.mp3', 'ai_data/can_13.mp3', 'ai_data/can_16.mp3', 'ai_data/can_21.mp3',
    'ai_data/can_27.mp3', 'ai_data/can_45.mp3', 'ai_data/can_46.mp3',
    # can

]

mfcc_train = []
mfcc_test = []
x_train = []
y_train = np.array(
    [[1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0],
     # bottle
     [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0],
     # bottle
     [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0],
     # bottle
     [1, 0, 0], [1, 0, 0],
     # bottle

     [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0],
     # pet
     [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0],
     # pet
     [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0],
     # pet
     [0, 1, 0],  # pet

     [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1],
     # can
     [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1],
     # can
     [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1],
     # can
     [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1],
     # can
     [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1],
     # can
     # can
     ]
)

x_test = []

y_test = np.array(
    [
        [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0],
        [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0], [0, 1, 0],
        [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1], [0, 0, 1]
    ]
)


# stft mean getting Short-time fourier transform.
def extract_feature(file_name):
    X, sample_rate = librosa.load(file_name)
    #X_stret = librosa.effects.time_stretch(X, 0.5)
    #X_pitch = librosa.effects.pitch_shift(X_stret, sample_rate, n_steps=4)
    # these function is used to improve feature_extracting but now, accuracy is 100% so i din't use yet.
    stft = np.abs(librosa.stft(X))  # abs function make return mean magnitude.
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
    mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
    return mfccs, chroma, mel, contrast, tonnetz


for i in train_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    x_train.append(ext_features)

for i in test_list:
    mfccs, chroma, mel, contrast, tonnetz = extract_feature(i)
    ext_features = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
    x_test.append(ext_features)

X = tf.placeholder(tf.float32)
Y = tf.placeholder(tf.float32)

W1 = tf.Variable(tf.random_uniform([193, 30], -1., 1.))
W2 = tf.Variable(tf.random_uniform([30, 3], -1., 1.))

b1 = tf.Variable(tf.zeros([30]))
b2 = tf.Variable(tf.zeros([3]))

L1 = tf.add(tf.matmul(X, W1), b1)
L1 = tf.nn.relu(L1)

model = tf.add(tf.matmul(L1, W2), b2)

cost = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits(labels=Y, logits=model))

optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
train_op = optimizer.minimize(cost)

init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)

for step in range(300):
    sess.run(train_op, feed_dict={X: x_train, Y: y_train})

    if (step + 1) % 10 == 0:
        print(step + 1, sess.run(cost, feed_dict={X: x_train, Y: y_train}))

prediction = tf.argmax(model, 1)
target = tf.argmax(Y, 1)
predict = sess.run(prediction, feed_dict={X: x_test})
true = sess.run(target, feed_dict={Y: y_test})
#print('예측값:', sess.run(prediction, feed_dict={X: x_train}))
#print('실제값:', sess.run(target, feed_dict={Y: y_train}))
count = (predict == true)
print(count)
print(predict)
print(true)
print("++++++++++++++ Print Result ~! ++++++++++++")
print(((count.sum()-(count==False).sum())/count.sum())*100)
print("+++++++++++++++++++++++++++++++++++++++++++")